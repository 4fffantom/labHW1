const express = require('express');
const path = require('path');
const morgan = require('morgan')
const mongoose = require('mongoose');
const app = express();

const {notesRouter} = require('./controllers/notesController');
const {usersRouter} = require('./controllers/usersController'); 
const {authRouter} = require('./controllers/authController'); 
const {authMiddleware} = require('./middlewares/authMiddleware'); 
const {NodeCourseError} = require('./utils/errors'); 

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/notes', [authMiddleware], notesRouter);
app.use('/api/users/me', [authMiddleware], usersRouter);

app.use((req, res, next) => {
    res.status(404).json({message: 'Not found'})
});

app.use((err, req, res, next) => {
    if (err instanceof NodeCourseError) {
        return res.status(err.status).json({message: err.message});
    }
    res.status(500).json({message: err.message});
});

//mongodb+srv://julia:R2bPVf3kE8zLvPE@cluster0.0juii.mongodb.net/test?retryWrites=true&w=majority
const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://me:1234@database.jubr9.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
            useNewUrlParser: true, useUnifiedTopology: true
        });
        app.listen(8080);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
}

start();