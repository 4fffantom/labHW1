const {User} = require('../models/userModel');

const getUserById = async (userId) => {
    const user = await User.findOne({_id: userId});
    if(!user) {
        throw new Error('No such user');
    }
    return user;
}

const deleteUserById = async (userId) => {
    await User.findOneAndRemove({_id: userId});
}

const updatePasswordById = async (userId, password) => {
    await User.findOneAndUpdate({_id:userId}, { $set: {password}});
}

module.exports = {
    deleteUserById,
    updatePasswordById,
    getUserById
};