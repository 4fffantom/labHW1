const {Note} = require('../models/noteModel');

const getNotesByUserId = async (userId,offset,limit) => {
    const notes = await Note.find({userId});
    console.log(offset,limit)
    if(!offset) return notes
    return notes.slice(Number(offset),Number(offset)+Number(limit));
}

const getNoteByIdForUser = async (noteId, userId) => {
    const note = await Note.findOne({_id: noteId, userId});
    return note;
}

const addNoteToUser = async (userId, notePayload) => {
    const note = new Note({...notePayload, userId});
    await note.save();
}

const updateNoteByIdForUser = async (noteId, userId, data) => {
    await Note.findOneAndUpdate({_id: noteId, userId}, { $set: data});
}

const checkNoteByIdForUser = async (noteId, userId) => {
    const note = await getNoteByIdForUser(noteId,userId);
    const data = {
        completed:!note.completed
    }
    await Note.findOneAndUpdate({_id: noteId, userId}, { $set: data});
}

const deleteNoteByIdForUser = async (noteId, userId) => {
    await Note.findOneAndRemove({_id: noteId, userId});
}

module.exports = {
    getNotesByUserId,
    getNoteByIdForUser,
    addNoteToUser,
    updateNoteByIdForUser,
    deleteNoteByIdForUser,
    checkNoteByIdForUser
};